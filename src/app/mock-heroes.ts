import { Hero } from "./hero";

export const HEROES: Hero[] = [
    { id: 11, name: 'Camila'},
    { id: 12, name: 'Jorge'},
    { id: 13, name: 'Benjamín'},
    { id: 14, name: 'Simón'},
    { id: 15, name: 'Jacinta'}
]